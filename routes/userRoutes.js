const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid,deleteUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();



router.get('/',function(req,res){
    res.send(UserService.getAllUsers());
})
router.get('/:id',function(req,res){
    let id = req.params.id;
    id = id.slice(1,id.length);
    const getCurrentUser = UserService.search(id);
    if(getCurrentUser == null) {
        res.status(404).send("User was not found");
    }else{
        res.send(getCurrentUser);
    }
    
})


router.put('/:id',updateUserValid,function(req,res){
    let id = req.params.id;
    id = id.slice(1,id.length);
    const newUserUpdated = UserService.updateUser(id,req.body);
    console.log(newUserUpdated)
    if(newUserUpdated == null){
        res.status(404).send("User was not found");
    }else{
        res.send(newUserUpdated);
    }
  
})
router.delete('/:id',deleteUserValid,function(req,res){
    let id = req.params.id;
    id = id.slice(1,id.length);
    const deletedUser = UserService.deleteUser(id);
    res.send(deletedUser);
})
// TODO: Implement route controllers for user
router.post("/",createUserValid,function(req,res){
    const newUser = UserService.saveNewUser(req.body);
    res.send(newUser);
})

module.exports = router;