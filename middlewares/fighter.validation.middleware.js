const { fighter } = require('../models/fighter');
const FighterService = require("../services/fighterService");

class CheckValidFigter{

    checkDefense(val){
        if(val<10 && val >1) return true;
        return false;
    }

    checkPower(val){
        if(val>1 && val < 100)return true;
        return false;
    }

    checkHealth(val){
        if(val>80 && val<120) return true
        return false;
    }
    checkFighterValues(obj){
        let arrOfKeys = Object.keys(obj);
        let rightKeys = Object.keys(fighter);

        for(let key of arrOfKeys){
            if(!rightKeys.includes(key)) return false;
        }
        return true;
    }
}

const checkVal = new CheckValidFigter();

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    try{
        const findFighter =  FighterService.findCoincidence(req.body);
       
        if(findFighter) throw new Error("Fighter:Current Fighter was created");
        if(!checkVal.checkFighterValues(req.body)) throw new Error("Fighter:You shoudn`t add another key"); 
        if(req&&
            req.body&&
            req.body.name&&
            req.body.power&&
            req.body.defense)
            {
                const {power,defense,health} = req.body;
                if(!(checkVal.checkPower(power) && checkVal.checkDefense(defense))){
                    throw new Error("Fighter:Not valid values"); 
                }
                if(health){
                    if(!(checkVal.checkHealth(health))){
                        throw new Error("Fighter:Not valid values"); 
                    }
                }   
                next();     
            }else throw new Error("Fighter:Not full body")
    }catch(err){
        res.err = err;
        res.status(400).send({error:true,message:err.message});
    }
        
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    try{
        if(req.body.id) throw new Error("Fighter:You shouldn`t add id");
        if(!checkVal.checkFighterValues(req.body)) throw new Error("Fighter:You shoudn`t add another key"); 
        if(req&&
            req.body&&
            req.body.name ||
            req.body.power ||
            req.body.defense || 
            req.body.health){
                const {power,defense,health} = req.body;
                if(power){
                    if(!checkVal.checkPower(power))  throw new Error("Fighter:Not valid values");
                }
                if(defense){
                    if(!checkVal.checkDefense(defense))  throw new Error("Fighter:Not valid values");
                }
                if(health){
                    if(!checkVal.checkHealth(health))  throw new Error("Fighter:Not valid values");
                }
                next();
            }else throw new Error("Fighter:Not enough body")
    }catch(err){
        res.err = err;
        res.status(400).send({error:true,message:err.message});
    }
    
}

const deleteFighterValid = (req,res,next) => {
    try{
        let id = req.params.id;
        id = id.slice(1,id.length);
        if(FighterService.idCoincidence(id)){
            next();
        }else{
            throw new Error("Not valid id");
        }
        
    }catch(err){
        res.err = err;
        res.status(404).send({error:true,message:err.message});
    }
}
exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;
exports.deleteFighterValid = deleteFighterValid;