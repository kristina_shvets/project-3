const { user } = require('../models/user');
const UserService = require("../services/userService");
class CheckValid{
    
    emailValid(value){
        const regex = /^[\w.+\-]+@gmail\.com$/i;
        return regex.test(value)
    }

    phoneValid(value){
        const regex = /^\+380\d{3}\d{2}\d{2}\d{2}$/i;
        return regex.test(value)
    }

    passwordValid(value){
        if(value.length>=3) return true;
        return false;
    }
    checkUserValues(obj){
        let arrOfKeys = Object.keys(obj);
        let rightKeys = Object.keys(user);

        for(let key of arrOfKeys){
            if(!rightKeys.includes(key)) return false;
        }
        return true;
    }
}

const checkValid = new CheckValid();

const createUserValid = (req, res, next) => {
    
    
    // TODO: Implement validatior for user entity during creation
    try{
        const item = UserService.findCoincidence(req.body);
        if(item) throw new Error("Current user was created.Try to sign in!");
        if(!(checkValid.checkUserValues(req.body)))throw new Error("Invalid body for user")
        if(req&&
            req.body&&
            req.body.email&&
            req.body.firstName&&
            req.body.lastName&&
            req.body.password&&
            req.body.phoneNumber)
            {
                const {email,phoneNumber,password} = req.body;
                if(!(checkValid.emailValid(email)&&
                checkValid.phoneValid(phoneNumber)&&
                checkValid.passwordValid(password))){
                    throw new Error("User:Not valid values");

                }
                next();
            }else throw new Error("User:Not full body")
    }catch(err){
        res.err = err;
        res.status(400).send({error:true,message:err.message});
    }
   
}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update
    try{
       
        if(req.body.id) throw new Error("You sholdn`t add id")
        if(!(checkValid.checkUserValues(req.body))) throw new Error("Invalid body for user")
        if(req.body.email){
            if(!checkValid.emailValid(req.body.email)){
                throw new Error("User:User update email is not valid");
            } 
            next();
        }

        if(req.body.phoneNumber){
            if(!checkValid.phoneValid(req.body.phoneNumber)){
                throw new Error("User:User update phoneNumber is not valid");
            }
            next();
        }

        if(req.body.password){
            if(!checkValid.passwordValid(req.body.password)){
                throw new Error("User:User update password is not valid");
            }
            next();
        }
    }catch(err){
        res.err = err;
        res.status(400).send({error:true,message:err.message});
    }

}

const deleteUserValid = (req,res,next)=>{
    try{
        let id = req.params.id;
        id = id.slice(1,id.length);
        if(UserService.idCoincidence(id)){
            next();
        }else{
            throw new Error("Not valid id");
        }
        
    }catch(err){
        res.err = err;
        res.status(404).send({error:true,message:err.message});
    }
}
exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;
exports.deleteUserValid = deleteUserValid;